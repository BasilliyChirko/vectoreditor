import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

public class MainGUI extends JFrame {
    public JPanel rootPanel;
    public JPanel drawingPanel;
    public JPanel infoPanel;
    public JPanel toolsPanel;
    public JComboBox<Double> comboStep;
    private JRadioButton dragRadioButton;
    private JRadioButton mPointRadioButton;
    private JRadioButton lineRadioButton;
    private JRadioButton removeRadioButton;
    private JRadioButton curveRadioButton;
    public JTextPane textPane1;

    public MainGUI(int width, int height, Controller controller) {
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setContentPane(rootPanel);
        setSize(width, height);
        setTitle("VectorEditor");
        setLocationRelativeTo(null);
        setMinimumSize(new Dimension(400, 200));
        setVisible(true);

        textPane1.setFont(Controller.Companion.getFont16());

        comboStep.setFont(Controller.Companion.getFont16());
        comboStep.addItem(1.0);
        comboStep.addItem(0.5);
        comboStep.addItem(0.25);
        comboStep.addItem(0.1);
        comboStep.setSelectedIndex(1);

        ArrayList<JRadioButton> buttons = new ArrayList<>();
        buttons.add(dragRadioButton);
        buttons.add(mPointRadioButton);
        buttons.add(lineRadioButton);
        buttons.add(removeRadioButton);
        buttons.add(curveRadioButton);

        dragRadioButton.setSelected(true);

        dragRadioButton.addActionListener(e -> controller.setMouseType("D"));
        mPointRadioButton.addActionListener(e -> controller.setMouseType("M"));
        lineRadioButton.addActionListener(e -> controller.setMouseType("L"));
        removeRadioButton.addActionListener(e -> controller.setMouseType("R"));
        curveRadioButton.addActionListener(e -> controller.setMouseType("C"));

        for (JRadioButton button : buttons) {
            button.setFont(Controller.Companion.getFont16());
        }

    }

}
