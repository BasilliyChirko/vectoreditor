import javax.swing.JPanel
import java.awt.*
import java.awt.event.MouseAdapter
import java.awt.event.MouseEvent

class InfoPanel(panel: JPanel, controller: Controller) : Panel(panel, controller) {
    var padding = Dimension(25, 25)

    private val click = object : MouseAdapter() {
        override fun mouseClicked(e: MouseEvent) {
            val x = e.x
            val y = e.y

            if (x in 0..125 && y in (panel.height - 36)..(panel.height - 20)) {
                Parser.toShapeShifter(controller.pathList)
            } else if (x in 0..40 && y in (panel.height - 60)..(panel.height - 45)) {
                Parser.savePath(controller.pathList)
            } else if (x in 46..85 && y in (panel.height - 60)..(panel.height - 45)) {
                Parser.loadPath()?.let { controller.setData(it) }
            }
        }
    }

    init {
        panel.addMouseListener(click)
        invalidate()
    }

    fun drawPath() {
        val path = controller.path
        draw {
            it.color = Controller.Companion.black
            it.font = Controller.Companion.font18
            val g = it
            path.data.forEachIndexed { i, (data) ->
                g.drawText(data, 0, 24 * i)
            }
        }
    }

    override fun invalidate(){
        clean()
        drawPath()
        printPath()
        drawLabels()
        pushImage()
    }

    private fun drawLabels() {
        draw {
            it.color = Controller.blueLight
            it.font = Controller.font16
            it.drawString("To ShapeShifter", 0, panel.height - 25)

            it.drawString("Save  Load", 0, panel.height - 50)
        }
    }

    private fun printPath() {
        val builder = StringBuilder()

        controller.path.data.forEach {
            builder.append(it.data).append(' ')
        }

        controller.gui.textPane1.text = builder.toString()
    }

    override fun clean() {
        draw {
            it.color = Controller.greyLight
            it.fillRect(0, 0, image.width, image.height)
        }
    }

    fun Graphics.drawText(s: String, x: Int, y: Int) {
        this.drawString(s, x, y + padding.height)
    }
}