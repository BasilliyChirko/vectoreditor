import java.awt.*
import java.awt.event.KeyAdapter
import java.awt.event.KeyEvent
import java.util.*
import kotlin.collections.ArrayList

class Controller {

    companion object {
        val greyDark = Color(0x888888)
        val greyNormal = Color(0xC0C0C0)
        val greyLight = Color(0xEEEEEE)
        val white = Color(0xF5F5F5)
        val blueLight = Color(0x2222FF)
        val green = Color(0x22BB22)
        val red = Color(0xBB2222)
        val black = Color(0x101010)
        val font16 = Font("Arial", 1, 16)
        val font18 = Font("Arial", 1, 18)
        val font12 = Font("Arial", 1, 12)


        fun getCubicPoint(t: Double, p0: Point, p1: Point, p2: Point, p3: Point): Point {
            val q0 = getSquarePoint(t, p0, p1, p2)
            val q1 = getSquarePoint(t, p1, p2, p3)
            val x = (q1.x - q0.x) * t + q0.x
            val y = (q1.y - q0.y) * t + q0.y
            return Point(x, y)
        }

        fun getSquarePoint(t: Double, p0: Point, p1: Point, p2: Point): Point {
            val q0 = getLinearPoint(t, p0, p1)
            val q1 = getLinearPoint(t, p1, p2)
            val x = (q1.x - q0.x) * t + q0.x
            val y = (q1.y - q0.y) * t + q0.y
            return Point(x,y)
        }

        fun getLinearPoint(t: Double, p0: Point, p1: Point): Point {
            return Point((p1.x - p0.x) * t + p0.x, (p1.y - p0.y) * t + p0.y)
        }

        var inst: Controller? = null
    }

    val gui: MainGUI
    val drawPanel: DrawPanel
    val infoPanel: InfoPanel
    val toolsPanel: ToolsPanel
    var path: Path
    var pathList: ArrayList<Path>


    init {
        path = Path()
        pathList = ArrayList()
        pathList.add(path)


        inst = this
        gui = MainGUI(1080, 620, this)
        drawPanel = DrawPanel(gui.drawingPanel, this)
        infoPanel = InfoPanel(gui.infoPanel, this)
        toolsPanel = ToolsPanel(gui.toolsPanel, this)

        gui.comboStep.addActionListener {
            drawPanel.stickStep = gui.comboStep.selectedItem as Double
        }

        gui.textPane1.addKeyListener(object : KeyAdapter(){
            override fun keyPressed(e: KeyEvent) {
                if (e.keyCode == KeyEvent.VK_ENTER) {
                    setInputPath()
                }
            }
        })

//        path = Path()
//        pathList.add(path)
//
//        path.data.add(Command("M 12.0 10.0"))
//        path.data.add(Command("C 16.0 4.0 20.0 8.0 14.0 12.0"))
//        path.data.add(Command("C 20.0 16.0 16.0 20.0 12.0 14.0"))
//        path.data.add(Command("C 8.0 20.0 4.0 16.0 10.0 12.0"))
//        path.data.add(Command("C 4.0 8.0 8.0 4.0 12.0 10.0"))

        invalidate()
    }

    fun setData(list: ArrayList<Path>) {
        pathList = list
        path = pathList[0]
        invalidate()
    }

    fun setPath(index: Int) {
        path = pathList[index]
        invalidate()
    }

    fun addPath() {
        pathList.add(Path())
        invalidate()
    }

    fun removePath(index: Int) {
        pathList.removeAt(index)
        invalidate()
    }

    fun invalidate() {
        drawPanel.invalidate()
        infoPanel.invalidate()
        toolsPanel.invalidate()
    }

    fun setInputPath() {
        Parser.parsePath(gui.textPane1.text)?.let {
            val index = pathList.indexOf(path)
            pathList.remove(path)
            path = it
            pathList.add(index, path)
            invalidate()
        }
    }

    fun setMouseType(actionCommand: String) {
        when (actionCommand) {
            "D" -> drawPanel.mouseType = DrawPanel.MouseType.DRAG
            "M" -> drawPanel.mouseType = DrawPanel.MouseType.MOVE
            "L" -> drawPanel.mouseType = DrawPanel.MouseType.LINE
            "R" -> drawPanel.mouseType = DrawPanel.MouseType.REMOVE
            "C" -> drawPanel.mouseType = DrawPanel.MouseType.CURVE
        }
        drawPanel.invalidate()
    }

    fun movePathPoint(command: Command, x: Int, y: Int) {
        val dpX = drawPanel.toDp((x - drawPanel.padding.width))
        val dpY = drawPanel.toDp((y - drawPanel.padding.height))
        if (dpX in 0..24 && dpY in 0..24)
            if (command.type == CommandType.CURVE) {
                val split = command.data.split(" ")
                command.data = split[0] + " " + split[1] + " " + split[2] + " " + split[3] + " " + split[4] + " " + dpX + " " + dpY
            } else {
                command.data = command.data[0] + " " + dpX + " " + dpY
            }
    }

    fun moveCurveSubpoint1(command: Command, x: Int, y: Int) {
        val dpX = drawPanel.toDp((x - drawPanel.padding.width))
        val dpY = drawPanel.toDp((y - drawPanel.padding.height))
        if (dpX in 0..24 && dpY in 0..24) {
                val split = command.data.split(" ")
                command.data = split[0] + " " + dpX + " " + dpY + " " + split[3] + " " + split[4] + " " + split[5] + " " + split[6]
            }
    }

    fun moveCurveSubpoint2(command: Command, x: Int, y: Int) {
        val dpX = drawPanel.toDp((x - drawPanel.padding.width))
        val dpY = drawPanel.toDp((y - drawPanel.padding.height))
        if (dpX in 0..24 && dpY in 0..24) {
            val split = command.data.split(" ")
            command.data = split[0] + " " + split[1] + " " + split[2] + " " + dpX + " " + dpY + " " + split[5] + " " + split[6]
        }
    }

    fun getPathPoint(x: Int, y: Int): Command? {
        val dpX = drawPanel.toDp((x - drawPanel.padding.width))
        val dpY = drawPanel.toDp((y - drawPanel.padding.height))
        path.data.filter { it.type != CommandType.ZONE }.minBy {
            val point = it.getPoint()
            Math.sqrt(Math.pow(point.x - dpX, 2.0) + Math.pow(point.y - dpY, 2.0))
        }?.let {
            val point = it.getPoint()
            if (Math.sqrt(Math.pow(point.x - dpX, 2.0) + Math.pow(point.y - dpY, 2.0)) < 0.8) {
                return it
            }
        }
        return null
    }

    fun getCurveSubpoint1(x: Int, y: Int): Command? {
        val dpX = drawPanel.toDp((x - drawPanel.padding.width))
        val dpY = drawPanel.toDp((y - drawPanel.padding.height))
        path.data.filter { it.type == CommandType.CURVE }.minBy {
            val point = it.getCurvePoints()
            Math.sqrt(Math.pow(point.first.x - dpX, 2.0) + Math.pow(point.first.y - dpY, 2.0))
        }?.let {
            val point = it.getCurvePoints()
            if (Math.sqrt(Math.pow(point.first.x - dpX, 2.0) + Math.pow(point.first.y - dpY, 2.0)) < 0.8) {
                return it
            }
        }
        return null
    }

    fun getCurveSubpoint2(x: Int, y: Int): Command? {
        val dpX = drawPanel.toDp((x - drawPanel.padding.width))
        val dpY = drawPanel.toDp((y - drawPanel.padding.height))
        path.data.filter { it.type == CommandType.CURVE }.minBy {
            val point = it.getCurvePoints()
            Math.sqrt(Math.pow(point.second.x - dpX, 2.0) + Math.pow(point.second.y - dpY, 2.0))
        }?.let {
            val point = it.getCurvePoints()
            if (Math.sqrt(Math.pow(point.second.x - dpX, 2.0) + Math.pow(point.second.y - dpY, 2.0)) < 0.8) {
                return it
            }
        }
        return null
    }

    fun removePathPoint(command: Command) {
        path.data.let {
            val index = it.indexOf(command)

            if (index + 1 < it.size) {
                if (it[index + 1].type != CommandType.ZONE) {
                    it[index + 1].type = CommandType.MOVE
                }
            }

            it.remove(command)
            removeZoneCollizion(it)
        }
    }

    fun removeLine(x: Int, y: Int) {
        val dpX = drawPanel.toDp((x - drawPanel.padding.width))
        val dpY = drawPanel.toDp((y - drawPanel.padding.height))


        getLinePoint(Point(dpX, dpY))?.let {
            log(it)

            when (it.type) {
                CommandType.NONE -> {}
                CommandType.MOVE -> {}
                CommandType.LINE -> {
                    it.type = CommandType.MOVE
                    val command = it

                    path.data.let {
                        val data = it
                        val index = data.indexOf(command)
                        data.filter { data.indexOf(it) > index }.forEach {
                            if (it.type == CommandType.MOVE) {
                                return@forEach
                            }

                            if (it.type == CommandType.ZONE) {
                                data.remove(it)
                                return@forEach
                            }
                        }
                    }
                }
                CommandType.CURVE -> {
                    it.type = CommandType.MOVE
                    val command = it

                    path.data.let {
                        val data = it
                        val index = data.indexOf(command)
                        data.filter { data.indexOf(it) > index }.forEach {
                            if (it.type == CommandType.MOVE) {
                                return@forEach
                            }

                            if (it.type == CommandType.ZONE) {
                                data.remove(it)
                                return@forEach
                            }
                        }
                    }
                }
                CommandType.ZONE -> {
                    path.data.remove(it)
                }
            }

            path.data.let { removeZoneCollizion(it) }
        }
    }

    fun isPointLineable(command: Command?): Boolean {
        if (command == null) return false
        return when (command.type) {
            CommandType.NONE -> false
            CommandType.ZONE -> false
            CommandType.CURVE -> {
                val forward = path.data.forward(command)
                forward == null || forward.type == CommandType.MOVE
            }
            CommandType.MOVE -> true
            CommandType.LINE -> {
                val forward = path.data.forward(command)
                forward == null || forward.type == CommandType.MOVE
            }
        }
    }

    private fun getLinePoint(click: Point): Command? {
        var last = Point(0.0, 0.0)

        path.data.minBy {
            var value = Double.MAX_VALUE

            when (it.type) {
                CommandType.MOVE -> last = it.getPoint()
                CommandType.LINE -> {
                    val point = it.getPoint()
                    value = lineDistance(click, point, last)
                    last = point
                }
                CommandType.ZONE -> {
                    val index = path.data.indexOf(it)
                    val point = path.data.filter { it.type == CommandType.MOVE && path.data.indexOf(it) < index }.last().getPoint()
                    value = lineDistance(click, point, last)
                    last = point
                }
                CommandType.CURVE -> {
                    val point = it.getPoint()
                    val cPoints = it.getCurvePoints()

                    var min = pointDistance(point, click)
                    for (i in 0..100) {
                        val d = pointDistance(getCubicPoint(i.toDouble() / 100.0, last, cPoints.first, cPoints.second, point), click)
                        if (min > d) {
                            min = d
                        }
                    }
                    value = min
                    last = point
                }
                CommandType.NONE -> {}
            }

            value
        }?.let {
            when (it.type) {
                CommandType.MOVE -> return null
                CommandType.CURVE -> {
                    val point = it.getPoint()
                    val point1 = path.data[path.data.indexOf(it) - 1].getPoint()

                    val cPoints = it.getCurvePoints()

                    var min = pointDistance(point, click)
                    for (i in 0..100) {
                        val d = pointDistance(getCubicPoint(i.toDouble() / 100.0, point1, cPoints.first, cPoints.second, point), click)
                        if (min > d) {
                            min = d
                        }
                    }

                    if (min < 0.8)
                        return it
                    return null
                }
                CommandType.LINE -> {
                    val point = it.getPoint()
                    val point1 = path.data[path.data.indexOf(it) - 1].getPoint()
                    if (lineDistance(click, point, point1) < 0.8)
                        return it
                    return null
                }
                CommandType.ZONE -> {
                    val index = path.data.indexOf(it)
                    val point1 = path.data[path.data.indexOf(it) - 1].getPoint()
                    val point = path.data.filter { it.type == CommandType.MOVE && path.data.indexOf(it) < index }.last().getPoint()
                    if (lineDistance(click, point, point1) < 0.8)
                        return it
                    return null
                }
                CommandType.NONE -> return null
            }
        }

        return null
    }

    private fun lineDistance(p: Point, p0: Point, p1: Point): Double {
        return Math.abs(((p0.y - p1.y) * p.x + (p1.x -  p0.x) * p.y + (p0.x * p1.y - p1.x * p0.y)) / Math.sqrt(Math.pow((p1.x - p0.x), 2.0) + Math.pow((p1.y - p0.y), 2.0)))
    }

    private fun pointDistance(p: Point, p1: Point): Double {
        return Math.sqrt(Math.pow(p.x - p1.x, 2.0) + Math.pow(p.y - p1.y, 2.0))
    }



    fun removeZoneCollizion(data: ArrayList<Command>) {
        data.filter {
            val index = data.indexOf(it) - 2
            if (index > -1) {
                it.type == CommandType.ZONE && data[index].type == CommandType.MOVE
            } else false
        }.forEach {
            data.remove(it)
        }
    }

    fun addMovePathPoint(x: Int, y: Int) {
        val dpX = drawPanel.toDp((x - drawPanel.padding.width))
        val dpY = drawPanel.toDp((y - drawPanel.padding.height))
        path?.data?.add(Command("M $dpX $dpY"))
    }

}