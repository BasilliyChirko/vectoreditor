import java.awt.*
import java.awt.event.*
import javax.swing.JPanel

class DrawPanel(panel: JPanel, controller: Controller) : Panel(panel, controller) {


    enum class MouseType {
        DRAG, MOVE, LINE, REMOVE, CURVE
    }

    var mouseType = MouseType.DRAG
    var stickStep = 0.5

    val pointBig = 1.0
    val pointSmall = 0.6

    var borderWidth = 50
    var padding = Dimension(0, 0)
    var dimen = 1.0
    var size: Int = 0

    var x = 0
    var y = 0

    var draggedPoint: Command? = null
    var draggedCurveSubpoint1: Command? = null
    var draggedCurveSubpoint2: Command? = null

    var linePoint1: Command? = null


    init {
        panel.addMouseMotionListener(object : MouseMotionAdapter() {
            override fun mouseMoved(e: MouseEvent) {
                super.mouseMoved(e)
                x = e.x
                y = e.y
                invalidate()
            }

            override fun mouseDragged(e: MouseEvent) {
                super.mouseDragged(e)
                x = e.x
                y = e.y
                draggedPoint?.let {
                    controller.movePathPoint(it, x, y)
                    invalidate()
                    controller.infoPanel.invalidate()
                }

                draggedCurveSubpoint1?.let {
                    controller.moveCurveSubpoint1(it, x, y)
                    invalidate()
                    controller.infoPanel.invalidate()
                }

                draggedCurveSubpoint2?.let {
                    controller.moveCurveSubpoint2(it, x, y)
                    invalidate()
                    controller.infoPanel.invalidate()
                }

                linePoint1?.let {
                    clean()
                    drawAllPath()
                    drawCurrentPath()
                    drawCurrentCoords()
                    val point = it.getPoint()
                    draw {
                        it.color = Controller.green
                        it.lineWigth(0.25)
                        val dpX = toDp((x - padding.width))
                        val dpY = toDp((y - padding.height))
                        it.drawLine(point.x, point.y, dpX, dpY)
                    }
                    pushImage()
                }
            }
        })

        panel.addMouseListener(object : MouseAdapter(){
            override fun mousePressed(e: MouseEvent) {
                super.mousePressed(e)
                when (mouseType) {
                    MouseType.DRAG -> {
                        draggedPoint = controller.getPathPoint(e.x, e.y)
                        if (draggedPoint == null) {
                            draggedCurveSubpoint1 = controller.getCurveSubpoint1(e.x, e.y)
                            if (draggedCurveSubpoint1 == null) {
                                draggedCurveSubpoint2 = controller.getCurveSubpoint2(e.x, e.y)
                            }
                        }
                    }
                    MouseType.REMOVE -> {
                        val pathPoint = controller.getPathPoint(e.x, e.y)
                        if (pathPoint != null) {
                            controller.removePathPoint(pathPoint)
                        } else {
                            controller.removeLine(e.x, e.y)
                        }
                        invalidate()
                        controller.infoPanel.invalidate()
                    }
                    MouseType.MOVE -> {
                        controller.addMovePathPoint(e.x, e.y)
                        invalidate()
                        controller.infoPanel.invalidate()
                    }
                    MouseType.LINE -> {
                        val command = controller.getPathPoint(e.x, e.y)
                        linePoint1 = if (controller.isPointLineable(command)) command else null
                    }
                    MouseType.CURVE -> {
                        val command = controller.getPathPoint(e.x, e.y)
                        linePoint1 = if (controller.isPointLineable(command)) command else null
                    }
                }
            }

            override fun mouseReleased(e: MouseEvent) {
                super.mouseReleased(e)
                draggedPoint = null
                draggedCurveSubpoint1 = null
                draggedCurveSubpoint2 = null

                if (linePoint1 != null) {
                    var linePoint2 = controller.getPathPoint(e.x, e.y)
                    if (controller.isPointLineable(linePoint2)) {

                        val first = controller.path.data.getFirst(linePoint1, linePoint2)

                        if (first == linePoint2) {
                            val t = linePoint1
                            linePoint1 = linePoint2
                            linePoint2 = t
                        }


                        if (mouseType == DrawPanel.MouseType.LINE) {
                            if (linePoint1!!.type == CommandType.MOVE && linePoint2!!.type == CommandType.MOVE) {
                                if (controller.path.data.forward(linePoint1)!!.type == CommandType.LINE) {
                                    linePoint1!!.type = CommandType.LINE
                                    controller.path.data.remove(linePoint2)
                                    controller.path.data.add(controller.path.data.indexOf(linePoint1!!), linePoint2)
                                } else if (controller.path.data.forward(linePoint1)!!.type == CommandType.CURVE) {
                                    linePoint1!!.type = CommandType.LINE
                                    controller.path.data.remove(linePoint2)
                                    controller.path.data.add(controller.path.data.indexOf(linePoint1!!), linePoint2)
                                } else {
                                    linePoint2.type = CommandType.LINE
                                    controller.path.data.remove(linePoint2)
                                    controller.path.data.add(controller.path.data.indexOf(linePoint1!!) + 1, linePoint2)
                                }
                            } else if (linePoint1!!.type == CommandType.MOVE && linePoint2!!.type == CommandType.LINE) {
                                if (controller.path.data.forward(linePoint1)!!.type == CommandType.LINE) {
                                    controller.path.data.add(controller.path.data.indexOf(linePoint2) + 1, Command("Z"))
                                } else if (controller.path.data.forward(linePoint1)!!.type == CommandType.CURVE) {
                                    controller.path.data.add(controller.path.data.indexOf(linePoint2) + 1, Command("Z"))
                                } else {
                                    linePoint1!!.type = CommandType.LINE
                                    controller.path.data.remove(linePoint1!!)
                                    controller.path.data.add(controller.path.data.indexOf(linePoint2) + 1, linePoint1!!)
                                }
                            } else if (linePoint1!!.type == CommandType.LINE && linePoint2!!.type == CommandType.MOVE) {
                                linePoint2.type = CommandType.LINE
                            } else if (linePoint1!!.type == CommandType.MOVE && linePoint2!!.type == CommandType.CURVE) {
                                if (controller.path.data.forward(linePoint1)!!.type == CommandType.LINE) {
                                    controller.path.data.add(controller.path.data.indexOf(linePoint2) + 1, Command("Z"))
                                } else if (controller.path.data.forward(linePoint1)!!.type == CommandType.CURVE) {
                                    controller.path.data.add(controller.path.data.indexOf(linePoint2) + 1, Command("Z"))
                                } else {
                                    linePoint1!!.type = CommandType.LINE
                                    controller.path.data.remove(linePoint1!!)
                                    controller.path.data.add(controller.path.data.indexOf(linePoint2) + 1, linePoint1!!)
                                }
                            } else if (linePoint1!!.type == CommandType.CURVE && linePoint2!!.type == CommandType.MOVE) {
                                linePoint2.type = CommandType.LINE
                            }
                        } else if (mouseType == DrawPanel.MouseType.CURVE) {
                            if (linePoint1!!.type == CommandType.MOVE && linePoint2!!.type == CommandType.MOVE) {
                                if (controller.path.data.forward(linePoint1)!!.type == CommandType.LINE) {
                                    linePoint1!!.type = CommandType.CURVE
                                    controller.path.data.remove(linePoint2)
                                    controller.path.data.add(controller.path.data.indexOf(linePoint1!!), linePoint2)
                                } else if (controller.path.data.forward(linePoint1)!!.type == CommandType.CURVE) {
                                    linePoint1!!.type = CommandType.CURVE
                                    controller.path.data.remove(linePoint2)
                                    controller.path.data.add(controller.path.data.indexOf(linePoint1!!), linePoint2)
                                } else {
                                    linePoint2.type = CommandType.CURVE
                                    controller.path.data.remove(linePoint2)
                                    controller.path.data.add(controller.path.data.indexOf(linePoint1!!) + 1, linePoint2)
                                }
                            } else if (linePoint1!!.type == CommandType.MOVE && linePoint2!!.type == CommandType.LINE) {
                                if (controller.path.data.forward(linePoint1)!!.type == CommandType.LINE) {
                                    controller.path.data.add(controller.path.data.indexOf(linePoint2) + 1, Command("Z"))
                                } else if (controller.path.data.forward(linePoint1)!!.type == CommandType.CURVE) {
                                    controller.path.data.add(controller.path.data.indexOf(linePoint2) + 1, Command("Z"))
                                } else {
                                    linePoint1!!.type = CommandType.CURVE
                                    controller.path.data.remove(linePoint1!!)
                                    controller.path.data.add(controller.path.data.indexOf(linePoint2) + 1, linePoint1!!)
                                }
                            } else if (linePoint1!!.type == CommandType.LINE && linePoint2!!.type == CommandType.MOVE) {
                                linePoint2.type = CommandType.CURVE
                            } else if (linePoint1!!.type == CommandType.MOVE && linePoint2!!.type == CommandType.CURVE) {
                                if (controller.path.data.forward(linePoint1)!!.type == CommandType.LINE) {
                                    controller.path.data.add(controller.path.data.indexOf(linePoint2) + 1, Command("Z"))
                                } else if (controller.path.data.forward(linePoint1)!!.type == CommandType.CURVE) {
                                    controller.path.data.add(controller.path.data.indexOf(linePoint2) + 1, Command("Z"))
                                } else {
                                    linePoint1!!.type = CommandType.CURVE
                                    controller.path.data.remove(linePoint1!!)
                                    controller.path.data.add(controller.path.data.indexOf(linePoint2) + 1, linePoint1!!)
                                }
                            } else if (linePoint1!!.type == CommandType.CURVE && linePoint2!!.type == CommandType.MOVE) {
                                linePoint2.type = CommandType.CURVE
                            }
                        }


                    }
                    linePoint1 = null
                    invalidate()
                    controller.infoPanel.invalidate()

                }

            }

        })

        initPanel()
        invalidate()
    }



    private fun drawAllPath() {
        controller.pathList.filter { it != controller.path }.forEach {
            drawPath(it, Controller.black, false)
        }
    }

    private fun drawCurrentPath() {
        drawPath(controller.path, Controller.blueLight, true)
    }

    private fun drawPath(path: Path, color: Color, drawControls: Boolean) {
        var last = Point(0.0, 0.0)
        draw {
            val g = it
            g.color = color

            g.lineWigth(0.25)
            path.data.forEachIndexed { index, command ->

                when (command.type) {
                    CommandType.MOVE -> {
                        last = command.getPoint()
                    }
                    CommandType.LINE -> {
                        val point = command.getPoint()
                        g.drawLine(last.x, last.y, point.x, point.y)
                        last = point
                    }
                    CommandType.ZONE -> {
                        val point = path.data.filter { it.type == CommandType.MOVE && path.data.indexOf(it) < index }.last().getPoint()
                        g.drawLine(last.x, last.y, point.x, point.y)
                        last = point
                    }
                    CommandType.CURVE -> {
                        val curvePoints = command.getCurvePoints()
                        val point = command.getPoint()

                        g.drawCurve(last, curvePoints.first, curvePoints.second, point)
                        last = point

                        if (drawControls) {
                            g.drawPoint(curvePoints.first, pointSmall)
                            g.drawPoint(curvePoints.second, pointSmall)
                        }
                    }
                    CommandType.NONE -> {
                    }
                }


                g.drawPoint(last, pointBig)
            }

            if (drawControls) {

                //draw other attributes
                if (mouseType == MouseType.LINE || mouseType == MouseType.CURVE) {  //such as point marker

                    path.data.filter { it.type != CommandType.ZONE }.forEach {
                        g.color = if (controller.isPointLineable(it)) Controller.green else Controller.red
                        g.drawPoint(it.getPoint(), pointSmall)
                    }

                } else {    // or point label
                    g.font = Controller.font12
                    g.color = Controller.white

                    path.data.filter { it.type != CommandType.ZONE }.forEachIndexed { index, command ->
                        val (x1, y1) = command.getPoint()
                        g.drawPointLabel((index + 1).toString(), x1, y1, pointBig)
                    }
                }
            }
        }
    }

    private fun drawCurrentCoords() {
        if (x in padding.width..(size + padding.width) && y in padding.height..(size + padding.height)) {
            draw {
                val dpX = (x - padding.width).dp()
                val dpY = (y - padding.height).dp()
                val intX = dpX.toInt()
                val intY = dpY.toInt()

                it.color = Controller.Companion.black
                it.font = Controller.Companion.font16
                it.drawString(intX.toString(),
                        (padding.width + intX * dimen).toInt() - borderWidth / 6,
                        padding.height - borderWidth / 8)
                it.drawString(intY.toStringWithForwardSpace(),
                        padding.width - 28,
                        (padding.height + intY * dimen).toInt() + borderWidth / 10)

                it.drawString("[$dpX;$dpY]", padding.width, panel.height - padding.height / 2)
            }
        }
    }

    override fun clean() = draw {
        // Main Back
        it.color = Controller.Companion.greyNormal
        it.fillRect(0, 0, panel.width, panel.height)

        // White Back
        it.color = Controller.Companion.white
        it.fillRect(padding.width, padding.height, size, size)

        // Lines
        it.color = Controller.Companion.greyDark
        it.lineWigth(0.1)
        for (i in 1..23) {
            it.drawLine(i.toDouble(), 0.0, i.toDouble(), 24.0)
        }
        for (i in 1..23) {
            it.drawLine(0.0, i.toDouble(), 24.0, i.toDouble())
        }

        // Coordinates
        it.color = Controller.Companion.greyDark
        it.font = Controller.Companion.font16


        val step = when (size) {
            in 0..250 -> 8
            in 251..500 -> 4
            in 501..1000 -> 2
            in 1001..9999 -> 1
            else -> 4
        }

        var number = step
        while (number <= 24) {
            it.drawString(number.toString(),
                    (padding.width + number * dimen).toInt() - borderWidth / 6,
                    padding.height - borderWidth / 8)
            it.drawString(number.toStringWithForwardSpace(),
                    padding.width - 28,
                    (padding.height + number * dimen).toInt() + borderWidth / 10)
            number += step
        }

    }

    override fun invalidate() {
        clean()
        drawCurrentCoords()
        drawAllPath()
        drawCurrentPath()
        pushImage()
    }

    //Initiate size and dimension of drawingPanel for drawing
    override fun initPanel() {
        borderWidth = ((if (panel.width < panel.height) panel.width else panel.height) * 2.0 / 30.0).toInt()
        size = (if (panel.width < panel.height) panel.width else panel.height) - borderWidth * 2
        padding = Dimension((panel.width - size) / 2, (panel.height - size) / 2)
        dimen = size.toDouble() / 24.0
    }


    fun Int.toStringWithForwardSpace(): String {
        return if (this < 10) " $this" else "$this"
    }

    fun Graphics.lineWigth(x: Double) {
        (this as Graphics2D).stroke = BasicStroke(x.px().toFloat())
    }

    fun Graphics.drawLine(x1: Double, y1: Double, x2: Double, y2: Double) {
        this.drawLine(x1.px() + padding.width, y1.px() + padding.height, x2.px() + padding.width, y2.px() + padding.height)
    }

    fun Graphics.drawLine(p1: Point, p2: Point) {
        this.drawLine(p1.x, p1.y, p2.x, p2.y)
    }

    fun Graphics.drawCurve(p0: Point, p1: Point, p2: Point, p3: Point) {
        var last = p0
        for (i in 2..100 step 2) {
            val point = Controller.getCubicPoint(i.toDouble() / 100.0, p0, p1, p2, p3)
            drawLine(last, point)
            last = point
        }
    }

    fun Graphics.drawPoint(x: Double, y: Double, size: Double) {
        val px = size.px()
        this.fillOval(x.px() + padding.width - px /2, y.px() + padding.height - px /2, px, px)
    }

    fun Graphics.drawPoint(point: Point, size: Double) {
        val (x, y) = point
        this.drawPoint(x, y, size)
    }

    fun Graphics.drawPointLabel(text: String, x: Double, y: Double, size: Double) {
        val px = size.px()
        if (text.length > 1) {
            this.drawString(text, x.px() + padding.width - (px.toDouble() * 0.4).toInt(), y.px() + padding.height + px /4)
        } else {
            this.drawString(text, x.px() + padding.width - px /4, y.px() + padding.height + px /4)
        }
    }

    fun Double.px(): Int = (this * dimen).toInt()

    fun Int.dp(): Double {
        val step = 1.0 / stickStep
        return Math.round(this.toDouble() * step / dimen).toDouble() / step
    }

    fun toDp(x: Int): Double = x.dp()

    fun toPx(x: Double): Int = x.px()



}