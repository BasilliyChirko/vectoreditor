
import java.io.*
import javax.swing.JFileChooser
import javax.swing.filechooser.FileNameExtensionFilter


class Parser {
    companion object {

        fun toShapeShifter(pathList: ArrayList<Path>) {
            val chooser = JFileChooser()
            chooser.currentDirectory = File(".")
            chooser.dialogTitle = "Save as"
            chooser.fileSelectionMode = JFileChooser.SAVE_DIALOG
            chooser.fileFilter = FileNameExtensionFilter("ShapeShifter", ".shapeshifter")

            if (chooser.showSaveDialog(null) === JFileChooser.APPROVE_OPTION) {
                try {
                    val file = File(chooser.selectedFile.toString() + ".shapeshifter")

                    val writer = BufferedWriter(FileWriter(file))
                    writer.write(parseFullPathShapeShifter(pathList))
                    writer.flush()
                    writer.close()
                } catch (ex: Exception) {
                    ex.printStackTrace()
                }
            } else {
                println("No Selection ")
            }
        }

        private fun parseFullPathShapeShifter(pathList: ArrayList<Path>): String  {
            val builder = StringBuilder()

            builder .append("{\n")
                    .append("  \"version\": 1,\n")
                    .append("  \"layers\": {\n")
                    .append("    \"vectorLayer\": {\n")
                    .append("      \"id\": \"1\",\n")
                    .append("      \"name\": \"vector\",\n")
                    .append("      \"type\": \"vector\",\n")
                    .append("      \"children\": [\n")

            pathList.forEachIndexed { index, path ->
                builder .append("        {\n")
                        .append("          \"id\": \"${index + 2}\",\n")
                        .append("          \"name\": \"path_${index + 1}\",\n")
                        .append("          \"type\": \"path\",\n")
                        .append("          \"pathData\": \"")
                        .append(parsePath(path))
                        .append("\"\n")
                        .append("        },")
            }

            builder.replace(builder.length - 1, builder.length, "")
            builder .append("\n      ]\n")
                    .append("    },\n")
                    .append("    \"hiddenLayerIds\": []\n")
                    .append("  },\n")
                    .append("  \"timeline\": {\n")
                    .append("    \"animation\": {\n")
                    .append("      \"id\": \"1\",\n")
                    .append("      \"name\": \"anim\",\n")
                    .append("      \"duration\": 300,\n")
                    .append("      \"blocks\": []\n")
                    .append("    }\n")
                    .append("  }\n")
                    .append("}")

            return builder.toString()
        }

        private fun parsePath(path: Path): String {
            val builder = StringBuilder()

            path.data.forEach {
                builder.append(it.data).append(' ')
            }

            var string = builder.toString()
            string = string.substring(0, string.length - 1)
            return string
        }

        fun savePath(pathList: ArrayList<Path>) {

            val chooser = JFileChooser()
            chooser.currentDirectory = File(".")
            chooser.dialogTitle = "Save as"
            chooser.fileSelectionMode = JFileChooser.SAVE_DIALOG
            chooser.fileFilter = FileNameExtensionFilter("VEditor", ".veditor")

            if (chooser.showSaveDialog(null) === JFileChooser.APPROVE_OPTION) {
                try {
                    val writer = BufferedWriter(FileWriter(File(chooser.selectedFile.toString() + ".veditor")))
                    pathList.forEach {
                        writer.write(parsePath(it))
                        writer.newLine()
                        writer.flush()
                    }
                    writer.close()
                } catch (ex: Exception) {
                    ex.printStackTrace()
                }
            } else {
                println("No Selection ")
            }
        }

        fun loadPath(): ArrayList<Path>? {

            val chooser = JFileChooser()
            chooser.currentDirectory = File(".")
            chooser.dialogTitle = "Load"
            chooser.fileSelectionMode = JFileChooser.FILES_ONLY
            chooser.fileFilter = FileNameExtensionFilter("VEditor", ".veditor")

            if (chooser.showSaveDialog(null) === JFileChooser.APPROVE_OPTION) {
                return try {
                    val list = ArrayList<Path>()
                    val reader = BufferedReader(FileReader(chooser.selectedFile))
                    reader.forEachLine {
                        parsePath(it)?.let { list.add(it) }
                    }
                    if (list.isNotEmpty()) list else null
                } catch (e: IOException) {
                    e.printStackTrace()
                    null
                }
            } else {
                println("No Selection ")
                return null
            }
        }

        private fun parseFullPathFile(pathList: ArrayList<Path>): String  {
            val builder = StringBuilder()
            pathList.forEach { builder.append(parsePath(it)).append("\n") }
            return builder.toString()
        }

        fun parsePath(text: String?): Path? {
            if (text == null || text.isEmpty()) return null

            val path = Path()
            val list = text.replace("  ", " ").toUpperCase().split(" ")

            try {

                var command = Command("")
                val iterator = list.iterator()
                while (iterator.hasNext()) {
                    when (iterator.next()) {
                        "M" -> {
                            command.data = "M " + iterator.next().toDouble() + " " + iterator.next().toDouble()
                        }
                        "L" -> {
                            command.data = "L " + iterator.next().toDouble() + " " + iterator.next().toDouble()
                        }
                        "C" -> {
                            command.data = "C " + iterator.next().toDouble() + " " + iterator.next().toDouble() + " " + iterator.next().toDouble() + " " + iterator.next().toDouble() + " " + iterator.next().toDouble() + " " + iterator.next().toDouble()
                        }
                        "Z" -> {
                            command.data = "Z"
                        }
                    }
                    if (command.data.isNotEmpty())
                        path.data.add(command)
                    command = Command("")
                }

            } catch(e: Exception) {
                return null
            }

            return path
        }


    }
}