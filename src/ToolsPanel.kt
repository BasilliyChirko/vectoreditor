import javax.swing.JPanel
import java.awt.Graphics
import java.awt.event.MouseAdapter
import java.awt.event.MouseEvent

class ToolsPanel(panel: JPanel, controller: Controller) : Panel(panel, controller) {

    private val click = object : MouseAdapter() {
        override fun mouseClicked(e: MouseEvent) {
            val x = e.x
            val y = e.y

            if (x in 8..78 && y in 10..20) {
                controller.addPath()
            } else {

                controller.pathList.forEachIndexed { index, _ ->
                    if (x in 8..60 && y in (index * 20 + 40)..(index * 20 + 40 + 16)) {
                        controller.setPath(index)
                        return
                    }
                    if (controller.pathList.size > 1)
                        if (x in 80..90 && y in (index * 20 + 40)..(index * 20 + 40 + 16)) {
                            controller.removePath(index)
                            return
                        }
                }

            }

        }
    }

    init {
        panel.addMouseListener(click)
        invalidate()
    }

    override fun invalidate(){
        clean()
        drawPathList()
        drawCurrentPath()
        pushImage()
    }

    override fun clean() {
        draw {
            it.color = Controller.greyLight
            it.fillRect(0, 0, image.width, image.height)

            it.color = Controller.black
            it.font = Controller.font16
            it.drawString("Add path", 8, 20)
        }
    }

    fun drawPathList() {
        draw {
            it.color = Controller.black
            it.font = Controller.font16
            if (controller.pathList.size == 1) {
                it.drawPathNumber(1, false)
            } else controller.pathList.forEachIndexed { index, _ ->
                it.drawPathNumber(index + 1, true)
            }
        }
    }

    fun drawCurrentPath() {
        draw {
            it.color = Controller.blueLight
            it.font = Controller.font16
            it.drawPathNumber(controller.pathList.indexOf(controller.path) + 1, controller.pathList.size != 1)
        }
    }

    fun Graphics.drawPathNumber(number: Int, drawX: Boolean) {
        this.drawString("Path_$number", 8, number * 20 + 30)
        if (drawX) this.drawString("X", 80, number * 20 + 30)
    }

}