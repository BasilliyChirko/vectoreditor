fun Any.log(any: Any?) {
    println(any)
}

fun Command.getPoint(): Point {
    val split = data.split(" ")
    if (type == CommandType.CURVE) {
        return Point(split[5].toDouble(), split[6].toDouble())
    }
    return Point(split[1].toDouble(), split[2].toDouble())
}

fun Command.getCurvePoints(): Pair<Point, Point> {
    val split = data.split(" ")
    return Pair(Point(split[1].toDouble(), split[2].toDouble()), Point(split[3].toDouble(), split[4].toDouble()))
}

fun <E> List<E>.forward(x: E): E? {
    val index = this.indexOf(x)
    if (index + 1 < this.size)
        return this[index + 1]
    return null
}

fun <E> List<E>.prev(x: E): E? {
    val index = this.indexOf(x)
    if (index - 1 > -1)
        return this[index - 1]
    return null
}

fun <E> List<E>.getFirst(x1: E?, x2: E?): E? {
    if (x1 == null || x2 == null) return null

    val i1 = indexOf(x1)
    val i2 = indexOf(x2)

    if (i1 < 0 || i2 < 0) return null

    return if (i1 < i2) x1 else x2
}
