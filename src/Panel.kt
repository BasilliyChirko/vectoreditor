import javax.swing.JPanel
import Controller
import java.awt.Graphics2D
import java.awt.RenderingHints
import java.awt.event.ComponentAdapter
import java.awt.event.ComponentEvent
import java.awt.image.BufferedImage

abstract class Panel(
        protected val panel: JPanel,
        protected val controller: Controller) {

    protected val image = BufferedImage(1080, 1080, BufferedImage.TYPE_INT_RGB)

    init {
        panel.addComponentListener(object : ComponentAdapter() {
            override fun componentResized(e: ComponentEvent?) {
                super.componentResized(e)
                initPanel()
                invalidate()
            }
        })
    }

    abstract fun invalidate()
    abstract fun clean()


    open fun initPanel(){}

    protected fun pushImage() {
        panel.graphics.drawImage(image, 0, 0, null)
    }

    protected inline fun draw(call:(Graphics2D)->Unit) {
        val g = image.graphics as Graphics2D
        g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON)
        g.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON)
        call.invoke(g)
    }
}