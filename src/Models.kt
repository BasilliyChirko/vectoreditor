import java.util.*

data class Path(var data: ArrayList<Command> = ArrayList())

data class Command(
        var data: String
) {
    var type: CommandType
        get() = when (data[0]) {
            'M' -> CommandType.MOVE
            'L' -> CommandType.LINE
            'Z' -> CommandType.ZONE
            'C' -> CommandType.CURVE
            else -> CommandType.NONE
        }
        set(value) {
            when (value) {
                CommandType.NONE -> {
                }
                CommandType.MOVE -> {
                    val (x, y) = getPoint()
                    data = "M $x $y"
                }
                CommandType.LINE -> {
                    val (x, y) = getPoint()
                    data = "L $x $y"
                }
                CommandType.CURVE -> {

                    var point1 = Point(0.0, 0.0)
                    Controller.inst?.path?.data?.prev(this)?.let { point1 = it.getPoint() }
                    val point2 = getPoint()
                    val (x1, y1) = Controller.getLinearPoint(0.33, point1, point2)
                    val (x2, y2) = Controller.getLinearPoint(0.67, point1, point2)
                    val (x3, y3) = point2
                    data = "C $x1 $y1 $x2 $y2 $x3 $y3"
                }
                CommandType.ZONE -> data = "Z"
            }
        }
}

data class Point(var x: Double, var y: Double)

enum class CommandType {NONE, MOVE, LINE, ZONE, CURVE }

